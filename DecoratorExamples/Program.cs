﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DecoratorExamples
{
    class Program
    {
        public class Bll
        {
            private readonly IDal _dal;

            public Bll(IDal dal)
            {
                _dal = dal ?? throw new ArgumentNullException(nameof(dal));
            }

            public void DoBusiness()
            {
                int j = _dal.ProvideData();
                j++;
                _dal.SaveData(j);
            }
        }

        public interface IDal
        {
            int ProvideData();
            void SaveData(int data);
        }

        public class Dal : IDal
        {
            private int Data;
            public int ProvideData()
            {
                return Data;
            }

            public void SaveData(int data)
            {
                Data = data;
            }
        }

        public class LoggingDal : IDal
        {
            private readonly IDal _dal;

            public LoggingDal(IDal dal)
            {
                _dal = dal;
            }

            public int ProvideData()
            {
                Console.WriteLine($"{nameof(ProvideData)} called: {DateTime.UtcNow}");
                return _dal.ProvideData();
            }

            public void SaveData(int data)
            {
                Console.WriteLine($"{nameof(SaveData)} called: {DateTime.UtcNow}");
                _dal.SaveData(data);
            }
        }

        public class CryptoDal : IDal
        {
            private readonly IDal _dal;

            public CryptoDal(IDal dal)
            {
                _dal = dal;
            }

            public int ProvideData()
            {
                return _dal.ProvideData() ^ 55;
            }

            public void SaveData(int data)
            {
                _dal.SaveData(data ^ 55);
            }
        }
        static void Main(string[] args)
        {
            //LINQ example
            //new List<int>().Where(x => x % 2 == 0);
            //Enumerable.Where(new List<int>(), x => x % 2 == 0);
        }
    }
}
