﻿using System;

namespace FactoryMethod
{
    public interface IKavesPoharModul
    {
        IKavesPohar CreateKavesPohar();
    }

    public class DragaKavesPoharModul : IKavesPoharModul
    {
        public IKavesPohar CreateKavesPohar()
        {
            return new DragaKavesPohar();
        }
    }
    public class OlcsoKavesPoharModul : IKavesPoharModul
    {
        public IKavesPohar CreateKavesPohar()
        {
            return new OlcsoKavesPohar();
        }
    }

    public interface IKavesPohar
    {
        void KavetTarol();
    }

    public class DragaKavesPohar : IKavesPohar
    {
        public void KavetTarol()
        {
            Console.WriteLine(":) :)");
        }
    }
    public class OlcsoKavesPohar : IKavesPohar
    {
        public void KavetTarol()
        {
            Console.WriteLine(":( :(");
        }
    }

    public abstract class KaveFozoGep
    {
        public IKavesPoharModul PoharDispenser { get; set; }

        public KaveFozoGep(IKavesPoharModul poharDispenser)
        {
            PoharDispenser = poharDispenser;
        }

        public void KavetFoz()
        {
            VizetForral();
            KavetDaral();
            var pohar = PoharDispenser.CreateKavesPohar();//<-Abstract Factory azért mert Strategy-zált (konstruktorból kapja meg a factory methodod megvalósító osztályt)
            Osszefoz();
            pohar.KavetTarol();
            Izesit();
        }
        
        protected abstract void Izesit();
        protected abstract void Osszefoz();
        protected abstract void KavetDaral();
        protected abstract void VizetForral();
    }

    public class DragaKaveFozoGep : KaveFozoGep
    {
        protected override void Izesit()
        {
            Console.WriteLine("Drágán izesit");
        }

        protected override void Osszefoz()
        {
            Console.WriteLine("Drágán összefőz");
        }

        protected override void KavetDaral()
        {
            Console.WriteLine("Kerámiakés");
        }

        protected override void VizetForral()
        {
            Console.WriteLine("83 C-ra");
        }

        public DragaKaveFozoGep(IKavesPoharModul poharDispenser) : base(poharDispenser)
        {
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var kaveFozoGep2 = new DragaKaveFozoGep(new DragaKavesPoharModul());
            kaveFozoGep2.KavetFoz();
            kaveFozoGep2.KavetFoz();
            Console.ReadKey();
        }
    }
}