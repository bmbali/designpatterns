﻿using System;
using System.Collections.Generic;

namespace Observer
{
    public interface INotification
    {
        void Notify();
    }

    public class EmailNotification : INotification
    {
        public void Notify()
        {
            Console.WriteLine("Email notification");
        }
    }
    public class PhoneNotification : INotification
    {
        public void Notify()
        {
            Console.WriteLine("Phone notification");
        }
    }

    public class BankAccount
    {
        public int Balance { get; private set; }
        private readonly List<INotification> _notificationStrategies;

        public BankAccount(int balance)
        {
            Balance = balance;
            _notificationStrategies = new List<INotification>();
        }

        public void Attach(INotification notification)
        {
            _notificationStrategies.Add(notification);
        }
        public void Detach(INotification notification)
        {
            _notificationStrategies.Remove(notification);
        }

        protected virtual void Notify()
        {
            foreach (var s in _notificationStrategies)
            {
                s.Notify();
            }
        }

        public void Deposit(int amount)
        {
            Balance += amount;
            Notify();
        }

        public void Withdraw(int amount)
        {
            Balance -= amount;
            Notify();
        }
    }

    /*
     * Eseménykezelés tervezési minta == Observer
     */

    class Program
    {
        static void Main(string[] args)
        {
            var bankAccount = new BankAccount(0);
            bankAccount.Attach(new EmailNotification());
            bankAccount.Attach(new PhoneNotification());

            bankAccount.Deposit(500);
            bankAccount.Withdraw(200);
            Console.ReadKey();
        }
    }
}
