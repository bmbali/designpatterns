﻿using System;

namespace Adapter
{
    public class Szoba
    {
        private IElektromos Konnektor { get; set; }

        public Szoba(IElektromos eszkoz)
        {
            Konnektor = eszkoz;
        }

        public void AramotFelkapcsol()
        {
            Konnektor.Mukodik();
        }
    }

    public class OlaszTelefonTolto : IOlaszElektromos
    {
        public void Funckzione()
        {
            Console.WriteLine("Olasz telefontolto működik");
        }
    }

    public interface IElektromos
    {
        void Mukodik();
    }

    public interface IOlaszElektromos
    {
        void Funckzione();
    }

    public class Konverter : IElektromos
    {
        private readonly IOlaszElektromos _connectore;

        public Konverter(IOlaszElektromos connectore)
        {
            _connectore = connectore;
        }

        public void Mukodik()
        {
            _connectore?.Funckzione();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var olaszTelefonTolto = new OlaszTelefonTolto();
            var konverter = new Konverter(olaszTelefonTolto);
            Szoba sz = new Szoba(konverter);
            sz.AramotFelkapcsol();
            Console.ReadKey();
        }
    }
}