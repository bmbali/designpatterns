﻿using System;
using System.Text;

namespace Builder
{
    public class ConnectionStringBuilder
    {
        private bool IntegratedSecurity { get; set; }
        private string Server { get; }
        private string Database { get; set; }
        private string UserId { get; set; }
        private string Password { get; set; }

        public ConnectionStringBuilder(string server)
        {
            if(string.IsNullOrEmpty(server))
                throw new ArgumentException("Server must not be empty");
            Server = server;
        }

        public ConnectionStringBuilder WithDatabase(string database)
        {
            Database = database;
            return this;
        }
        public ConnectionStringBuilder WithIntegratedSecurity(bool isIntegratedSecurity)
        {
            IntegratedSecurity = isIntegratedSecurity;
            return this;
        }
        public ConnectionStringBuilder WithUserId(string userId)
        {
            UserId = userId;
            return this;
        }
        public ConnectionStringBuilder WithPassword(string password)
        {
            Password = password;
            return this;
        }

        public string GetConnectionString()
        {
            var sb = new StringBuilder();
            sb.Append($"Server={Server}");
            sb.Append($"Initial Catalog={(string.IsNullOrEmpty(Database) ? "master" : Database)}");
            if (IntegratedSecurity)
            {
                sb.Append($"Integrated Security=true");
            }
            else
            {
                if (string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(Password))
                {
                    throw new InvalidOperationException("UserID and password must be set when windows auth is off");
                }
            }

            return sb.ToString();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            //string connectionString = "Server=192.168.0.1;Database:testdb;Integrated Security=True";

            var connectionString = new ConnectionStringBuilder("192.168.0.1")
                .WithDatabase("testdb")
                .WithIntegratedSecurity(true)
                .WithUserId("Balazs")
                .WithPassword("Balazs")
                .GetConnectionString();

            Console.WriteLine(connectionString);
            Console.ReadKey();
        }
    }
}
