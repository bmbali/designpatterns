﻿using System;
using System.Threading;

namespace Receiver
{
    public class BusinessLogic
    {
        public static void DoBusiness1()
        {
            Console.WriteLine("Do static business");
        }

        public static void DoBusiness2(string input)
        {
            Console.WriteLine("Do static business with " + input);
        }

        public void DoBusiness3()
        {
            Console.WriteLine("Do real business");
        }
    }

    public interface ICommand
    {
        void Execute();
    }

    public class DoBusiness1Command : ICommand
    {
        public void Execute()
        {
            BusinessLogic.DoBusiness1();
        }
    }
    public class DoBusiness2Command : ICommand
    {
        private readonly string _input;
        public DoBusiness2Command(string input)
        {
            _input = input;
        }
        public void Execute()
        {
            BusinessLogic.DoBusiness2(_input);
        }
    }

    public class DoBusiness3Command : ICommand
    {
        private readonly BusinessLogic _receiver;
        public DoBusiness3Command(BusinessLogic receiver)
        {
            _receiver = receiver;
        }
        public void Execute()
        {
            _receiver.DoBusiness3();
        }
    }

    public class Invoker
    {
        public void Invoke(ICommand command)
        {
            Console.WriteLine($"{command} invoked");
            try
            {
                command.Execute();
            }
            catch
            {
                Thread.Sleep(1000);
                command.Execute();
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //BusinessLogic.DoBusiness1();
            //BusinessLogic.DoBusiness2("Balazs");
            //var bl = new BusinessLogic();
            //bl.DoBusiness3();

            var command1 = new DoBusiness1Command();
            var command2 = new DoBusiness2Command("Balazs");
            var command3 = new DoBusiness3Command(new BusinessLogic());

            //command1.Execute();
            //command2.Execute();
            //command3.Execute();

            var invoker = new Invoker();
            invoker.Invoke(command1);
            invoker.Invoke(command2);
            invoker.Invoke(command3);

            Console.ReadKey();
        }
    }
}
