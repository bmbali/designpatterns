﻿using System;

namespace Strategy
{
    public interface IKavaDaraloKes
    {
        void Daral();
    }

    public class OlcsoKaveDaraloKes : IKavaDaraloKes
    {
        public void Daral()
        {
            Console.WriteLine("Műanyag késsel darál");
        }
    }
    public class DragaKaveDaraloKes : IKavaDaraloKes
    {
        public void Daral()
        {
            Console.WriteLine("Kerámia késsel darál");
        }
    }

    public class KaveFozoGep
    {
        public IKavaDaraloKes KaveDaralo { get; private set; }
        public KaveFozoGep(IKavaDaraloKes kaveDaralo)
        {
            KaveDaralo = kaveDaralo;
        }
        public void KavetFoz()
        {
            Console.WriteLine("Vizet melegit");
            KaveDaralo.Daral();
            Console.WriteLine("Osszefoz");
            Console.WriteLine("Izesit");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var kaveFozoGep = new KaveFozoGep(new OlcsoKaveDaraloKes());
            kaveFozoGep.KavetFoz();
            Console.ReadKey();
        }
    }
}
