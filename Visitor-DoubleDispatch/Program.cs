﻿using System;
using System.Collections.Generic;

namespace DoubleDispatch
{
    public abstract class Player
    {
        public abstract void Attack(Monster monster);
    }

    public class Warrior : Player
    {
        public override void Attack(Monster monster)
        {
            monster.ResolveAttack(this);
        }
    }

    public class Wizard : Player
    {
        public override void Attack(Monster monster)
        {
            monster.ResolveAttack(this);
        }
    }

    public abstract class Monster
    {
        public abstract void ResolveAttack(Wizard wizard);
        public abstract void ResolveAttack(Warrior warrior);
    }

    public class Dragon : Monster {
        public override void ResolveAttack(Wizard wizard)
        {
            Console.WriteLine("Dragon resolved attack from wizard");
        }

        public override void ResolveAttack(Warrior warrior)
        {
            Console.WriteLine("Dragon resolved attack from warrior");
        }
    }

    public class Ork : Monster
    {
        public override void ResolveAttack(Wizard wizard)
        {
            Console.WriteLine("Ork resolved attack from wizard");
        }

        public override void ResolveAttack(Warrior warrior)
        {
            Console.WriteLine("Ork resolved attack from warrior");
        }
    }

    public class Uruk : Monster
    {
        public override void ResolveAttack(Wizard wizard)
        {
            Console.WriteLine("Uruk resolved attack from wizard");
        }

        public override void ResolveAttack(Warrior warrior)
        {
            Console.WriteLine("Uruk resolved attack from warrior");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var monsterHorde = new List<Monster>
            {
                new Dragon(), new Ork(), new Uruk()
            };

            Player player = new Wizard();

            foreach (var monster in monsterHorde)
            {
                player.Attack(monster);
            }

            Console.ReadKey();
        }
    }
}
