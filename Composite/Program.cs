﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Composite
{
    public class Szoba
    {
        private IElektromos Konnektor { get; set; }

        public Szoba(IElektromos eszkoz)
        {
            Konnektor = eszkoz;
        }

        public void AramotFelkapcsol()
        {
            Konnektor.Mukodik();
        }
    }

    public class Televizio : IElektromos
    {
        public void Mukodik()
        {
            Console.WriteLine("Propaganda");
        }
    }

    public class MiniHuto : IElektromos
    {
        public void Mukodik()
        {
            Console.WriteLine("Hűűűűt");
        }
    }

    public class Eloszto : IElektromos, IEnumerable<IElektromos>
    {
        private readonly HashSet<IElektromos> _konnektorok = new HashSet<IElektromos>();
        public void Bedug(IElektromos eszkoz)
        {
            if (eszkoz == null)
            {
                throw new ArgumentNullException(nameof(eszkoz), "Nem lehet null az eszkoz");
            }

            _konnektorok.Add(eszkoz);
        }

        public void Mukodik()
        {
            foreach (var eszkoz in _konnektorok)
            {
                Task.Run(() => eszkoz.Mukodik());
            }
        }

        public IEnumerator<IElektromos> GetEnumerator()
        {
            return _konnektorok.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(IElektromos elektromos)
        {
            Bedug(elektromos);
        }
    }

    public interface IElektromos
    {
        void Mukodik();
    }

    class Program
    {
        static void Main(string[] args)
        {
            var televizio = new Televizio();
            var miniHuto = new MiniHuto();
            var eloszto = new Eloszto() {televizio, miniHuto};
            foreach (var eszkoz in eloszto)
            {
                //ez egy lehetőség az IEnumerable implementálása után
            }
            //eloszto.Bedug(televizio);
            //eloszto.Bedug(miniHuto);
            Szoba sz = new Szoba(eloszto);
            sz.AramotFelkapcsol();
            Console.ReadKey();
        }
    }
}