﻿using System;

namespace DecoratorPractice
{
    public interface IIntStream
    {
        int Read();
        void Write(int data);
    }

    public class FileINtStream : IIntStream
    {
        public virtual int Read()
        {
            throw new NotImplementedException();
        }

        public virtual void Write(int data)
        {
            throw new NotImplementedException();
        }
    }

    public class MemoryIntStream : IIntStream
    {
        public virtual int Read()
        {
            throw new NotImplementedException();
        }

        public virtual void Write(int data)
        {
            throw new NotImplementedException();
        }
    }

    public class ZippedMemoryStream : MemoryIntStream
    {
        public override int Read()
        {
            return base.Read() * 2;
        }

        public override void Write(int data)
        {
            int zippedData = data / 2;
            base.Write(zippedData);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

        }
    }
}
