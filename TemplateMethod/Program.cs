﻿using System;

namespace TemplateMethod
{
    public class DragaKaveFozoGepTest : DragaKaveFozoGep
    {
        protected override void Osszefoz()
        {
            //MOCK implementation
        }
    }

    public abstract class KaveFozoGep
    {
        public void KavetFoz()
        {
            VizetForral();
            KavetDaral();
            Osszefoz();
            Izesit();
        }

        protected abstract void Izesit();
        protected abstract void Osszefoz();
        protected abstract void KavetDaral();
        protected abstract void VizetForral();
    }

    public class DragaKaveFozoGep : KaveFozoGep
    {
        protected override void Izesit()
        {
            Console.WriteLine("Drágán izesit");
        }

        protected override void Osszefoz()
        {
            Console.WriteLine("Drágán összefőz");
        }

        protected override void KavetDaral()
        {
            Console.WriteLine("Kerámiakés");
        }

        protected override void VizetForral()
        {
            Console.WriteLine("83 C-ra");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var kaveFozoGep2 = new DragaKaveFozoGep();
            kaveFozoGep2.KavetFoz();
            Console.ReadKey();
        }
    }
}