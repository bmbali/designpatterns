﻿using System;

namespace ChainOfResponsibility
{
    public abstract class BankEmployee
    {
        private readonly BankEmployee _supervisor;

        protected BankEmployee(BankEmployee bankEmployee)
        {
            _supervisor = bankEmployee;
        }

        public bool ApproveLoan(int amount)
        {
            if (CanHandle(amount))
                return Handle(amount);
            return _supervisor.ApproveLoan(amount);
        }

        protected abstract bool CanHandle(int amount);
        protected abstract bool Handle(int amount);
    }

    public class Teller : BankEmployee
    {
        public Teller(Manager bankEmployee) : base(bankEmployee)
        {
        }

        protected override bool CanHandle(int amount)
        {
            return amount < 5000;
        }

        protected override bool Handle(int amount)
        {
            if ((amount + DateTime.Now.Day) % 2 == 0)
            {
                return true;
            }

            return false;
        }
    }

    public class Manager : BankEmployee
    {
        public Manager(Director bankEmployee) : base(bankEmployee)
        {
        }

        protected override bool CanHandle(int amount)
        {
            return amount < 10000;
        }

        protected override bool Handle(int amount)
        {
            if (((int)Math.Sqrt(amount * 1.5)) % 2 == 1)
            {
                return true;
            }

            return false;
        }
    }

    public class Director : BankEmployee
    {
        public Director() : base(null)
        {
        }

        protected override bool CanHandle(int amount)
        {
            return true;
        }

        protected override bool Handle(int amount)
        {
            if (new Random().Next() % 2 == 0)
            {
                return true;
            }
            return false;
        }
    }

    public class Bank
    {
        private readonly BankEmployee _teller = new Teller(new Manager(new Director()));
        public bool ApproveLoan(int amount)
        {
            return _teller.ApproveLoan(amount);
        }
    }

    /*
     * Elágazás helyettesítése Polimorfizmussal
     */

    class Program
    {
        static void Main(string[] args)
        {
            Bank bank = new Bank();
            bank.ApproveLoan(7644);
            bank.ApproveLoan(4634);
            bank.ApproveLoan(5435476);
        }
    }
}
