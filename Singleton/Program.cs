﻿using System;

namespace Singleton
{
    public class Singleton
    {
        /*
         * Lazy Singleton
         */
         private readonly static Lazy<Singleton> instance 
             = new Lazy<Singleton>(() => new Singleton(), true);
        public static Singleton Instance => instance.Value;




        /*
         * a CLR garantálja, hogy a static propertyből csak egy példány fog létrejönni,
         * ezért a static property-ben való példányosítás is helyes
         */
        //private readonly static Singleton instance = new Singleton();

        //public static Singleton Instance
        //{
        //    get { return instance; }
        //}

            

        /*
         * a legegyszerűbb szálbiztos megvalósítás
         */
        //private static volatile Singleton instance;
        //private static object lockObject = new object();

        //public static Singleton Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            lock (lockObject)
        //            {
        //                if (instance == null)
        //                {
        //                    instance = new Singleton();
        //                }
        //            }
        //        }

        //        return instance;
        //    }
        //}

        private Singleton() { }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var singleton = Singleton.Instance;
        }
    }
}
