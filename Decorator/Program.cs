﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    public class Szoba
    {
        private IElektromos Konnektor { get; set; }

        public Szoba(IElektromos eszkoz)
        {
            Konnektor = eszkoz;
        }

        public void AramotFelkapcsol()
        {
            Konnektor.Mukodik();
        }
    }

    public class Eloszto : IElektromos, IEnumerable<IElektromos>
    {
        private readonly HashSet<IElektromos> _konnektorok = new HashSet<IElektromos>();
        public void Bedug(IElektromos eszkoz)
        {
            if (eszkoz == null)
            {
                throw new ArgumentNullException(nameof(eszkoz), "Nem lehet null az eszkoz");
            }

            _konnektorok.Add(eszkoz);
        }

        public void Mukodik()
        {
            foreach (var eszkoz in _konnektorok)
            {
                Task.Run(() => eszkoz.Mukodik());
            }
        }

        public IEnumerator<IElektromos> GetEnumerator()
        {
            return _konnektorok.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(IElektromos elektromos)
        {
            Bedug(elektromos);
        }
    }

    public interface IElektromos
    {
        void Mukodik();
    }

    public class FogyasztasMero : IElektromos
    {
        private readonly IElektromos Konnektor;
        private int OraAllas = 0;
        public FogyasztasMero(IElektromos konnektor)
        {
            Konnektor = konnektor;
        }
        public void Mukodik()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    OraAllas++;
                    Console.WriteLine(OraAllas);
                    Thread.Sleep(1000);
                }
            });
            Konnektor?.Mukodik();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var fogyasztasMero = new FogyasztasMero(new Eloszto());
            Szoba sz = new Szoba(fogyasztasMero);
            sz.AramotFelkapcsol();
            Console.ReadKey();
        }
    }
}