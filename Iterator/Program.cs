﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iterator
{
    public interface IIterator
    {
        int Current { get; }
        bool MoveNext();
        void Reset();
    }

    public interface IAggregate
    {
        IIterator GetIterator();
    }

    public class MaxEightElementLinearIntCollection : IAggregate
    {
        private HashSet<int> _elements  = new HashSet<int>();
        public int Count { get; set; }
        public void Add(int element)
        {
            if (Count == 8) throw new InvalidOperationException("Collection already full");

            _elements.Add(element);
            Count++;
        }

        public IIterator GetIterator()
        {
            return new MaxEightElementLinearIntCollectionIterator(this);
        }
        private class MaxEightElementLinearIntCollectionIterator : IIterator
        {
            private readonly MaxEightElementLinearIntCollection _source;

            public MaxEightElementLinearIntCollectionIterator(MaxEightElementLinearIntCollection source)
            {
                _source = source;
            }

            private int _pointer = -1;
            public int Current => _source._elements.ElementAt(_pointer);

            public bool MoveNext()
            {
                if (_pointer >= _source._elements.Count - 1)
                    return false;

                _pointer++;
                return true;
            }

            public void Reset()
            {
                _pointer = -1;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var l = new MaxEightElementLinearIntCollection();
            l.Add(5);
            l.Add(8);
            l.Add(10);

            var i = l.GetIterator();
            while (i.MoveNext())
            {
                Console.WriteLine(i.Current);
            }

            Console.ReadKey();
        }
    }
}
