﻿using System;

namespace ConsoleApp4
{
    public class A { }
    public class B : A { }

    public class C
    {
        public virtual void M(A a) { }
        public virtual void M(B b) { }
    }

    public class D : C
    {
        public override void M(A a) { }
        public override void M(B b) { }
    }

    class Program
    {
        static void Main(string[] args)
        {
            A a = new B();
            C c = new D();
            c.M(a);
        }
    }
}
