﻿using System;

namespace DependencyInversion
{
    public class Szoba
    {
        private IElektromos Konnektor { get; set; }

        public Szoba(IElektromos eszkoz)
        {
            Konnektor = eszkoz;
        }

        public void AramotFelkapcsol()
        {
            Konnektor.Mukodik();
        }
    }

    public class Televizio : IElektromos
    {
        public void Mukodik()
        {
            Console.WriteLine("Propaganda");
        }
    }

    public class MiniHuto : IElektromos
    {
        public void Mukodik()
        {
            Console.WriteLine("Hűűűűt");
        }
    }

    public interface IElektromos
    {
        void Mukodik();
    }


    /*
     * Open-Closed Principle
     * Single Responsibility
     * Dependency Inversion
     * SOLID
     * Dependency Injection (Struktúrált Függőség Kezelés)
     */

    class Program
    {
        static void Main(string[] args)
        {
            //var televizio = new Televizio();
            var miniHuto = new MiniHuto();
            Szoba sz = new Szoba(miniHuto);
            sz.AramotFelkapcsol();
            Console.ReadKey();
        }
    }
}
