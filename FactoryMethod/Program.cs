﻿using System;

namespace FactoryMethod
{
    public interface IKavesPohar
    {
        void KavetTarol();
    }

    public class DragaKavesPohar : IKavesPohar
    {
        public void KavetTarol()
        {
            Console.WriteLine(":) :)");
        }
    }
    public class OlcsoKavesPohar : IKavesPohar
    {
        public void KavetTarol()
        {
            Console.WriteLine(":( :(");
        }
    }

    public abstract class KaveFozoGep
    {
        public void KavetFoz()
        {
            VizetForral();
            KavetDaral();
            var pohar = CreateKavesPohar();     //<-Factory method
            Osszefoz();
            pohar.KavetTarol();
            Izesit();
        }

        protected abstract IKavesPohar CreateKavesPohar();
        protected abstract void Izesit();
        protected abstract void Osszefoz();
        protected abstract void KavetDaral();
        protected abstract void VizetForral();
    }

    public class DragaKaveFozoGep : KaveFozoGep
    {
        protected override IKavesPohar CreateKavesPohar()
        {
            return new DragaKavesPohar();
        }

        protected override void Izesit()
        {
            Console.WriteLine("Drágán izesit");
        }

        protected override void Osszefoz()
        {
            Console.WriteLine("Drágán összefőz");
        }

        protected override void KavetDaral()
        {
            Console.WriteLine("Kerámiakés");
        }

        protected override void VizetForral()
        {
            Console.WriteLine("83 C-ra");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var kaveFozoGep2 = new DragaKaveFozoGep();
            kaveFozoGep2.KavetFoz();
            kaveFozoGep2.KavetFoz();
            Console.ReadKey();
        }
    }
}